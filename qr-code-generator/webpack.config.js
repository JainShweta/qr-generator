module.exports = {
    target: 'webworker',
    entry: './index.js',
    mode: 'production',
    output: {
      path: __dirname+"/dist",
      publicPath: 'dist',
      filename: "main.js"
    },
  };